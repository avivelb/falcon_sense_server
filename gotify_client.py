import requests
import json


class GotifyClient:

    def __init__(self, base_url, client_token, verify_ssl=True):
        """
        Gotify client for Gotify server management Initializer.
        :param client_token: One of the client tokens of the app.
        :type client_token: C{str}.
        :param base_url: The base url, including protocol (http/https) and port if different than 80/443.
        :type base_url: C{str}.
        :param verify_ssl: verify tls/ssl certificate or not.
        :type verify_ssl: C{bool}.
        """
        self.base_url = base_url
        self.requests_session = requests.Session()
        if not verify_ssl:
            import urllib3
            urllib3.disable_warnings()
        self.requests_session.verify = verify_ssl
        self.requests_session.headers = {'X-Gotify-Key': client_token, 'content-type': 'application/json'}

    def create_client_token(self, client_name):
        """
        Method to create client token for management and receiving messages.
        :param client_name: The client name to be saved and shown in the website.
        :type client_name: C{str}
        :return: The request response.
        :rtype: C{dict}
        """
        return self.requests_session.post("{}/client".format(self.base_url),
                                          data=json.dumps({"name": client_name})).json()

    def get_client_tokens(self):
        return self.requests_session.get("{}/client".format(self.base_url)).json()
