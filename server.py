#!/usr/bin/env python3

# The authorization and central server that communicates with the Synology server
# And the Pi receiver, and the App.
from gotify_client import GotifyClient
import asyncio
import pathlib
import ssl
import select
import threading
import _thread
import signal
import websockets
import socket
import logging
import hashlib
import subprocess
import sys
import datetime
import requests
import json
import shelve
import time

try:
    import queue
except ImportError:
    import Queue as queue
# DB Imports
import sqlalchemy as sa
from sqla_wrapper import SQLAlchemy

# CONSTS AND GLOBALS
LOGFILE = "logs/{}.log".format(datetime.datetime.now().strftime("%d-%m-%Y"))
CACHE_FILE = "server_cache"

CACHE_IS_INSPECT_KEY = "is_inspect"
CACHE_ARM_DELAY = "arm_delay"
CACHE_DISARM_DELAY = "disarm_delay"
CACHE_ALARM_DELAY = "alarm_delay"
CACHE_TEMPERATURE = "temperature"
CACHE_HUMIDITY = "humidity"

PROTOCOL_AUTHORIZATION = "AUTH#"
PROTOCOL_PHYSICAL_DETECTION = "PHYS#"
PROTOCOL_WIRELESS_DETECTION = "WIRL#"
PROTOCOL_ADD_PHYSICAL_SENSOR = "ADDPHYS#"
PROTOCOL_ADD_WIRELESS_SENSOR = "ADDWIRL#"
PROTOCOL_DEL_PHYSICAL_SENSOR = "DELPHYS#"
PROTOCOL_DEL_WIRELESS_SENSOR = "DELWIRL#"
PROTOCOL_ENABLE_ALARM = "ENALARM#"
PROTOCOL_DISABLE_ALARM = "DISALARM#"
PROTOCOL_TEMPERATURE = "TEMP#"

PROTOCOL_AUTHORIZATION_ERROR = "AUTH_ERR"
PROTOCOL_USER_GET_SENSORS = "GETSENS"
PROTOCOL_USER_EDIT_SENSORS = "EDITSEN"
PROTOCOL_USER_ADD_PHYSICAL_SENSOR = "ADDSENPHYS"
PROTOCOL_USER_DELETE_PHYSICAL_SENSOR = "DELSENPHYS"
PROTOCOL_USER_ADD_WIRELESS_SENSOR = "ADDSENWIRL"
PROTOCOL_USER_DELETE_WIRELESS_SENSOR = "DELSENWIRL"
PROTOCOL_USER_GET_GROUPS = "GETGRP"
PROTOCOL_USER_EDIT_GROUP_SENSORS = "EDITGRPSEN"
PROTOCOL_USER_ADD_GROUP = "ADDGRP"
PROTOCOL_USER_DELETE_GROUP = "DELGRP"
PROTOCOL_USER_TOGGLE_GROUP = "TGLGRP"
PROTOCOL_USER_GET_IS_INSPECT_MODE = "ISINSPCT"
PROTOCOL_USER_SET_INSPECT_MODE = "SETINSPCT"
PROTOCOL_USER_GET_ARM_DELAY = "GETARMDELAY"
PROTOCOL_USER_SET_ARM_DELAY = "SETARMDELAY"
PROTOCOL_USER_GET_ALARM_DELAY = "GETALARMDELAY"
PROTOCOL_USER_SET_ALARM_DELAY = "SETALARMDELAY"
PROTOCOL_USER_SCHEDULE_ARM = "SCHDARM"
PROTOCOL_USER_GET_SCHEDULE_DAILY = "GETSCHDAY"
PROTOCOL_USER_ADD_SCHEDULE_DAILY = "ADDSCHDAY"
PROTOCOL_USER_DELETE_SCHEDULE_DAILY = "DELSCHDAY"
PROTOCOL_USER_GET_HUMIDITY = "GETHUMID"
PROTOCOL_USER_ALARM = "ALARM"
PROTOCOL_USER_DISALARM = "DISALARM"
PROTOCOL_USER_RESPONSE_OK = "OK"
PROTOCOL_DELIMITER = "#"

NOTIFICATION_PRIORITY_REALTIME = 10
NOTIFICATION_PRIORITY_INSPECT_MODE = 9
NOTIFICATION_PRIORITY_DEBUG = 2
NOTIFICATION_PRIORITY_TESTING = 0

ALARM_MAX_SECONDS = 300
gpio_to_pin = {2: 3, 3: 5, 4: 7,
               5: 29, 6: 31, 7: 26,
               8: 24, 9: 21, 10: 19,
               11: 23, 12: 32, 13: 33,
               14: 8, 15: 10, 16: 36,
               17: 11, 18: 12, 19: 35,
               20: 38, 21: 40, 22: 15,
               23: 16, 24: 18, 25: 22,
               26: 37, 27: 13}
WIRELESS_GPIO = 4

logging.basicConfig(filename=LOGFILE,
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%d/%m/%Y-%H:%M:%S',
                    level=logging.INFO)
HOST = "INSERT_ADDRESS_HERE"
RECEIVER_HOST = "INSERT_RECEIVER_ADDRESS_HERE"
SYNOLOGY_AUTH_URL = "https://{}:5001/webapi/auth.cgi".format(HOST)
GOTIFY_MESSAGING_TOKEN = "GOTIFY_MESSAGING_TOKEN"
GOTIFY_MANAGEMENT_TOKEN = "GOTIFY_MANAGEMENT_TOKEN"
logger = logging.getLogger("Authorization Sever")
logger.addHandler(logging.StreamHandler(sys.stdout))
db = SQLAlchemy('sqlite:///data.db')
gotify_client = GotifyClient("https://{}:2001".format(HOST), GOTIFY_MANAGEMENT_TOKEN, False)
cache = shelve.open(CACHE_FILE)
# Initialize caches' initial values for first use

if CACHE_IS_INSPECT_KEY not in cache:
    cache[CACHE_IS_INSPECT_KEY] = False

if CACHE_ARM_DELAY not in cache:
    # Arm delay in seconds
    cache[CACHE_ARM_DELAY] = 40

if CACHE_DISARM_DELAY not in cache:
    # Disarm delay in seconds
    cache[CACHE_DISARM_DELAY] = 40

if CACHE_ALARM_DELAY not in cache:
    # Alarm delay in seconds
    cache[CACHE_ALARM_DELAY] = 50

if CACHE_TEMPERATURE not in cache:
    # Temperature of system
    cache[CACHE_TEMPERATURE] = "0"

if CACHE_HUMIDITY not in cache:
    # Humidity of system
    cache[CACHE_HUMIDITY] = "0"

receiver_sockets = None
server = None
is_about_to_alarm = False
receiver_lock = _thread.allocate_lock()


class Sensor(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    name = db.Column(db.String(80), unique=True)
    gpio = db.Column(db.Integer)
    pin = db.Column(db.Integer)
    location = db.Column(db.String(80))
    sensor_type = db.Column(db.String(80))
    wireless_length = db.Column(db.Integer)

    def __init__(self, name, gpio, pin, location, sensor_type, wireless_length):
        self.name = name
        self.gpio = gpio
        self.pin = pin
        self.location = location
        self.sensor_type = sensor_type
        self.wireless_length = wireless_length

    def __repr__(self):
        return json.dumps({"Name": self.name, "GPIO": self.gpio, "Pin": self.pin, "Location": self.location,
                           "Sensor_Type": self.sensor_type, "Wireless_Length": self.wireless_length})


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    username = db.Column(db.String(80), unique=True)
    login_token = db.Column(db.String(100), unique=True)
    notification_token = db.Column(db.String(20), unique=True)
    pass_hash = db.Column(db.String(150), unique=True)

    def __init__(self, username, login_token, notification_token, pass_hash):
        self.username = username
        self.login_token = login_token
        self.notification_token = notification_token
        self.pass_hash = pass_hash

    def __repr__(self):
        return json.dumps(
            {"Username": self.username, "Login_Token": self.login_token, "Notify_Token": self.notification_token,
             "Hash": self.pass_hash})


class Group(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    name = db.Column(db.String(80), unique=True)
    data = db.Column(db.String(4096))
    is_triggered = db.Column(db.Boolean)
    is_scheduled_arm = db.Column(db.Boolean)
    is_scheduled_disarm = db.Column(db.Boolean)
    is_alarming = db.Column(db.Boolean)

    def __init__(self, name, data):
        self.name = name
        self.data = data
        self.is_triggered = False
        self.is_scheduled_arm = False
        self.is_scheduled_disarm = False
        self.is_alarming = False

    def __repr__(self):
        return json.dumps({"Name": self.name, "Data": self.data.split(","), "Is_Triggered": self.is_triggered,
                           "Is_Scheduled_Arm": self.is_scheduled_arm, "Is_Scheduled_Disarm": self.is_scheduled_disarm,
                           "Is_Alarming": self.is_alarming})


class Scheduler(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    group_name = db.Column(db.String(80))
    day = db.Column(db.String(10))
    time = db.Column(db.String(10))
    action = db.Column(db.String(10))

    def __init__(self, group_name, day, time, action):
        self.group_name = group_name
        self.day = day
        self.time = time
        self.action = action

    def __repr__(self):
        return json.dumps({"Group_Name": self.group_name, "Day": self.day, "Time": self.time,
                           "Action": self.action})


db.create_all()


# for every successfully authenticated user in synology.

def sha512(plain):
    """
    :param plain: The text to hash
    :type plain: C{bytes}
    :return: Hashed
    :rtype: C{str}
    """
    return hashlib.sha512(plain).hexdigest()


def timed_schedule_arm(group, timeout=cache[CACHE_ARM_DELAY]):
    e = threading.Event()
    e.wait(timeout=timeout)
    arm_sensor_group_scheduled(group)


def arm_sensor_group_scheduled(group):
    global db
    search = db.query(Group).filter_by(name=group).first()
    if search is not None:
        search.is_triggered = True
        search.is_scheduled_arm = False
        if search.is_triggered:
            push_notification("Group Arm", "Sensors in group {} are armed".format(search.name),
                              get_current_priority())
        db.commit()


def timed_schedule_alarm(group, timeout=cache[CACHE_ALARM_DELAY]):
    e = threading.Event()
    e.wait(timeout=timeout)
    alarm_system_scheduled(group)


def alarm_system_scheduled(group):
    global db
    global is_about_to_alarm
    e = threading.Event()
    start_time = time.time()
    search = db.query(Group).filter_by(name=group).first()
    if search is not None and search.is_triggered:
        send_to_receivers(PROTOCOL_ENABLE_ALARM)
        search.is_alarming = True
        db.commit()
        while search is not None and search.is_triggered and (time.time() - start_time) < ALARM_MAX_SECONDS:
            db.refresh(search)
            search = db.query(Group).filter_by(name=group).first()
            e.wait(timeout=1)

        # Terminating alarm 3 times, just to be sure
        send_to_receivers(PROTOCOL_DISABLE_ALARM)
        send_to_receivers(PROTOCOL_DISABLE_ALARM)
        send_to_receivers(PROTOCOL_DISABLE_ALARM)
        search.is_alarming = False
        db.commit()
        is_about_to_alarm = False


# Idk why do I need the second parameter tbh
async def message_handler(websocket, _):
    global db
    global cache
    global gpio_to_pin
    msg = await websocket.recv()
    message_back = "ERR"
    logger.info("Got message {} from socket: {}".format(msg, websocket))
    # This is the authentication part with synology
    if msg.startswith(PROTOCOL_AUTHORIZATION):
        spl = msg.split(PROTOCOL_AUTHORIZATION)[1]
        username = spl.split(":")[0]
        password = spl.split(":")[1]
        user_search = db.query(User).filter_by(username=username.lower()).first()
        if user_search:
            logger.info(
                "Checking logging info for known user {} from socket: {}".format(username, websocket))
            if password == user_search.pass_hash:
                logger.info(
                    "Cache Auth for known user {} is success from socket: {}".format(username, websocket))

                # Auth success using DB, sending also the client token for notifications
                message_back = "{}{}#{}".format(PROTOCOL_AUTHORIZATION, user_search.login_token,
                                                user_search.notification_token)
            else:
                logger.info(
                    "Cache Auth for known user {} is failure due to wrong password from socket: {}".format(username,
                                                                                                           websocket))
                message_back = "{}{}".format(PROTOCOL_AUTHORIZATION, PROTOCOL_AUTHORIZATION_ERROR)
        # User does not exists, maybe it's a new Admin in synology we should add
        else:
            auth_result = await auth_with_synology(username, password)
            logger.info("Auth result {} from socket: {}".format(auth_result, websocket))

            # Auth success using synology
            if auth_result[0]:
                # Checking if there's already a token exists for the user

                existing_token_query = [entry["token"] for entry in gotify_client.get_client_tokens() if
                                        entry["name"] == "{}_client".format(username.lower())]

                if len(existing_token_query) > 0:
                    new_client_token = existing_token_query[0]

                # Else, create a request to get a new token
                else:
                    new_client_token = gotify_client.create_client_token("{}_client".format(username.lower()))["token"]
                if type(password) == str:
                    password = password.encode("UTF-8")
                pass_hash = sha512(password)
                db.add(User(username.lower(), auth_result[1], new_client_token, pass_hash))
                db.commit()
                # Auth success using synology, sending also the client token for notifications
                message_back = "{}{}#{}".format(PROTOCOL_AUTHORIZATION, user_search.login_token,
                                                new_client_token)
            # Auth failure
            else:
                logger.info(auth_result[0])
                message_back = "{}{}".format(PROTOCOL_AUTHORIZATION, PROTOCOL_AUTHORIZATION_ERROR)

    # The next messages are handled if the user authorized successfully
    else:
        spl = msg.split(PROTOCOL_DELIMITER)
        # Checks if the user has the token
        tokens = [json.loads(str(entry))["Login_Token"] for entry in db.query(User).all()]
        # User has a valid token, then he may request whatever
        if spl[0] in tokens:
            request = spl[1]
            if request == PROTOCOL_USER_GET_SENSORS:
                sensors = db.query(Sensor).all()
                logger.info("Sensors: {}".format(sensors))
                message_back = "{}#{}".format(PROTOCOL_USER_GET_SENSORS, sensors)

            elif request == PROTOCOL_USER_EDIT_SENSORS:
                search = db.query(Sensor).filter_by(gpio=spl[2]).first()
                if search:
                    # Change the name in the group as well if the senor is in a group
                    group_search = db.query(Group).all()
                    for group in group_search:
                        sensors_in_group = group.data.split(",")
                        if search.name in sensors_in_group:
                            sensors_in_group.remove(search.name)
                            sensors_in_group.append(spl[3])
                        group.data = ",".join(sensors_in_group)
                    search.name = spl[3]
                    search.location = spl[4]
                    db.commit()
                    message_back = "{}#{}".format(PROTOCOL_USER_EDIT_SENSORS, PROTOCOL_USER_RESPONSE_OK)
                else:
                    message_back = "{}#Sensor_Not_Found".format(PROTOCOL_USER_EDIT_SENSORS)
            elif request == PROTOCOL_USER_ADD_PHYSICAL_SENSOR:
                # Searching if GPIO not already present
                search = db.query(Sensor).filter_by(gpio=int(spl[4])).first()
                if search is None:
                    db.add(Sensor(spl[2], int(spl[4]), gpio_to_pin[int(spl[4])], spl[3], "Physical", 0))
                    db.commit()
                    send_to_receivers("{}{}".format(PROTOCOL_ADD_PHYSICAL_SENSOR, spl[4]))
                    message_back = "{}#{}".format(PROTOCOL_USER_ADD_PHYSICAL_SENSOR, PROTOCOL_USER_RESPONSE_OK)
                else:
                    message_back = "{}#Sensor_Already_Exists".format(PROTOCOL_USER_ADD_PHYSICAL_SENSOR)

            elif request == PROTOCOL_USER_DELETE_PHYSICAL_SENSOR:
                # Searching if GPIO exists
                search = db.query(Sensor).filter_by(gpio=int(spl[2])).first()
                if search is not None:
                    # Remove in the group as well if the sensor is in a group
                    group_search = db.query(Group).all()
                    for group in group_search:
                        sensors_in_group = group.data.split(",")
                        if search.name in sensors_in_group:
                            sensors_in_group.remove(search.name)
                        group.data = ",".join(sensors_in_group)
                    search.delete()
                    db.commit()
                    send_to_receivers("{}{}".format(PROTOCOL_DEL_PHYSICAL_SENSOR, spl[2]))
                    message_back = "{}#{}".format(PROTOCOL_USER_DELETE_PHYSICAL_SENSOR, PROTOCOL_USER_RESPONSE_OK)
                else:
                    message_back = "{}#Sensor_Not_Found".format(PROTOCOL_USER_DELETE_PHYSICAL_SENSOR)

            elif request == PROTOCOL_USER_ADD_WIRELESS_SENSOR:
                search = db.query(Sensor).filter_by(wireless_length=int(spl[4])).first()
                if search is None:
                    db.add(
                        Sensor(spl[2], WIRELESS_GPIO, gpio_to_pin[WIRELESS_GPIO], spl[3], "Wireless", int(spl[4])))
                    db.commit()
                    send_to_receivers("{}{}".format(PROTOCOL_ADD_WIRELESS_SENSOR, spl[4]))
                    message_back = "{}#{}".format(PROTOCOL_USER_ADD_WIRELESS_SENSOR, PROTOCOL_USER_RESPONSE_OK)
                else:
                    message_back = "{}#Sensor_Already_Exists".format(PROTOCOL_USER_ADD_WIRELESS_SENSOR)

            elif request == PROTOCOL_USER_DELETE_WIRELESS_SENSOR:
                # Searching if Wireless exists
                search = db.query(Sensor).filter_by(wireless_length=int(spl[2])).first()
                if search is not None:
                    group_search = db.query(Group).all()
                    for group in group_search:
                        sensors_in_group = group.data.split(",")
                        if search.name in sensors_in_group:
                            sensors_in_group.remove(search.name)
                        group.data = ",".join(sensors_in_group)
                    search.delete()
                    db.commit()
                    send_to_receivers("{}{}".format(PROTOCOL_DEL_WIRELESS_SENSOR, spl[2]))
                    message_back = "{}#{}".format(PROTOCOL_USER_DELETE_WIRELESS_SENSOR, PROTOCOL_USER_RESPONSE_OK)
                else:
                    message_back = "{}#Sensor_Not_Found".format(PROTOCOL_USER_DELETE_WIRELESS_SENSOR)

            elif request == PROTOCOL_USER_GET_GROUPS:
                # Searching if there are any groups available
                groups = db.query(Group).all()
                # print(groups)
                if groups is not None:
                    message_back = "{}#{}".format(PROTOCOL_USER_GET_GROUPS, groups)
                    logger.info("Groups: {}".format(groups))

            elif request == PROTOCOL_USER_ADD_GROUP:
                # Searching if the name not exists
                search = db.query(Group).filter_by(name=spl[2]).first()
                if search is None:
                    db.add(Group(spl[2], spl[3]))
                    db.commit()
                    message_back = "{}#{}".format(PROTOCOL_USER_ADD_GROUP, PROTOCOL_USER_RESPONSE_OK)
                else:
                    message_back = "{}#Group_Already_Exists".format(PROTOCOL_USER_DELETE_WIRELESS_SENSOR)

            elif request == PROTOCOL_USER_DELETE_GROUP:
                search = db.query(Group).filter_by(name=spl[2]).first()
                if search is not None:
                    search.delete()
                    db.commit()
                    message_back = "{}#{}".format(PROTOCOL_USER_DELETE_GROUP, PROTOCOL_USER_RESPONSE_OK)
                else:
                    message_back = "{}#Group_Not_Exists".format(PROTOCOL_USER_DELETE_WIRELESS_SENSOR)

            elif request == PROTOCOL_USER_TOGGLE_GROUP:
                search = db.query(Group).filter_by(name=spl[2]).first()
                if search is not None:
                    search.is_triggered = not search.is_triggered
                    if search.is_triggered:
                        threading.Thread(target=push_notification,
                                         args=("Group Arm",
                                               "Sensors in group {} are armed".format(search.name),
                                               get_current_priority()),
                                         ).start()
                    else:
                        threading.Thread(target=push_notification,
                                         args=("Group Disarm",
                                               "Sensors in group {} are unarmed".format(search.name),
                                               get_current_priority()),
                                         ).start()
                    print("Group is now {}".format(search.is_triggered))
                    db.commit()
                    message_back = "{}#{}".format(PROTOCOL_USER_TOGGLE_GROUP, PROTOCOL_USER_RESPONSE_OK)
                else:
                    message_back = "{}#Group_Not_Exists".format(PROTOCOL_USER_DELETE_WIRELESS_SENSOR)

            elif request == PROTOCOL_USER_EDIT_GROUP_SENSORS:
                # Searching if the name exists
                group = db.query(Group).filter_by(name=spl[2]).first()
                if group is not None:
                    group.data = spl[3]
                    db.commit()
                    message_back = "{}#{}".format(PROTOCOL_USER_EDIT_GROUP_SENSORS, PROTOCOL_USER_RESPONSE_OK)

            elif request == PROTOCOL_USER_GET_IS_INSPECT_MODE:
                message_back = "{}#{}".format(PROTOCOL_USER_GET_IS_INSPECT_MODE, cache[CACHE_IS_INSPECT_KEY])

            elif request == PROTOCOL_USER_SET_INSPECT_MODE:
                val = (spl[2] == "true")
                cache[CACHE_IS_INSPECT_KEY] = val
                cache.sync()
                message_back = "{}#{}".format(PROTOCOL_USER_SET_INSPECT_MODE, PROTOCOL_USER_RESPONSE_OK)

            # Handling arm delay get
            elif request == PROTOCOL_USER_GET_ARM_DELAY:
                message_back = "{}#{}".format(PROTOCOL_USER_GET_ARM_DELAY, str(cache[CACHE_ARM_DELAY]))

            # Handling arm delay set
            elif request == PROTOCOL_USER_SET_ARM_DELAY:
                val = int(spl[2])
                cache[CACHE_ARM_DELAY] = int(val)
                cache.sync()
                message_back = "{}#{}".format(PROTOCOL_USER_SET_ARM_DELAY, PROTOCOL_USER_RESPONSE_OK)

            # Handling alarm delay get
            elif request == PROTOCOL_USER_GET_ALARM_DELAY:
                message_back = "{}#{}".format(PROTOCOL_USER_GET_ALARM_DELAY, str(cache[CACHE_ALARM_DELAY]))

            # Handling alarm delay set
            elif request == PROTOCOL_USER_SET_ALARM_DELAY:
                val = int(spl[2])
                cache[CACHE_ALARM_DELAY] = int(val)
                cache.sync()
                message_back = "{}#{}".format(PROTOCOL_USER_SET_ALARM_DELAY, PROTOCOL_USER_RESPONSE_OK)

            elif request == PROTOCOL_USER_SCHEDULE_ARM:
                search = db.query(Group).filter_by(name=spl[2]).first()
                if search is not None:
                    search.is_scheduled = True
                    db.commit()
                    threading.Thread(target=timed_schedule_arm, args=(search.name,)).start()
                    threading.Thread(target=push_notification,
                                     args=("Group Schedule",
                                           "Sensors in group {} are scheduled to arm".format(search.name),
                                           get_current_priority()),
                                     ).start()

                    message_back = "{}#{}".format(PROTOCOL_USER_SCHEDULE_ARM, PROTOCOL_USER_RESPONSE_OK)
                else:
                    message_back = "{}#Group_Not_Exists".format(PROTOCOL_USER_SCHEDULE_ARM)
            elif request == PROTOCOL_USER_GET_SCHEDULE_DAILY:
                schedules = db.query(Scheduler).all()
                if schedules is not None:
                    message_back = "{}#{}".format(PROTOCOL_USER_GET_SCHEDULE_DAILY, schedules)
                else:
                    message_back = "{}#{}".format(PROTOCOL_USER_GET_SCHEDULE_DAILY, "Schema_Error")
            elif request == PROTOCOL_USER_ADD_SCHEDULE_DAILY:
                search_exists = db.query(Scheduler).filter_by(group_name=spl[2], day=spl[3], action=spl[4],
                                                              time=spl[5]).first()
                if search_exists:
                    message_back = "{}#{}".format(PROTOCOL_USER_GET_SCHEDULE_DAILY, "Already_Exists")
                else:
                    db.add(Scheduler(group_name=spl[2], day=spl[3], time=spl[5], action=spl[4]))
                    db.commit()
                    message_back = "{}#{}".format(PROTOCOL_USER_GET_SCHEDULE_DAILY, PROTOCOL_USER_RESPONSE_OK)
            elif request == PROTOCOL_USER_DELETE_SCHEDULE_DAILY:
                search_exists = db.query(Scheduler).filter_by(group_name=spl[2], day=spl[3], action=spl[4],
                                                              time=spl[5]).first()
                if search_exists:
                    search_exists.delete()
                    db.commit()
                    message_back = "{}#{}".format(PROTOCOL_USER_DELETE_SCHEDULE_DAILY, PROTOCOL_USER_RESPONSE_OK)
                else:
                    message_back = "{}#{}".format(PROTOCOL_USER_GET_SCHEDULE_DAILY, "Not_Exists")
            elif request == PROTOCOL_USER_ALARM:
                send_to_receivers(PROTOCOL_ENABLE_ALARM)
                message_back = "{}#{}".format(PROTOCOL_USER_ALARM, PROTOCOL_USER_RESPONSE_OK)
            elif request == PROTOCOL_USER_DISALARM:
                send_to_receivers(PROTOCOL_DISABLE_ALARM)
                send_to_receivers(PROTOCOL_DISABLE_ALARM)
                send_to_receivers(PROTOCOL_DISABLE_ALARM)
                message_back = "{}#{}".format(PROTOCOL_USER_DISALARM, PROTOCOL_USER_RESPONSE_OK)
            elif request == PROTOCOL_USER_GET_HUMIDITY:
                if CACHE_TEMPERATURE in cache and CACHE_HUMIDITY in cache:
                    message_back = "{}#{}#{}".format(PROTOCOL_USER_GET_HUMIDITY, cache[CACHE_TEMPERATURE],
                                                     cache[CACHE_HUMIDITY])
                else:
                    message_back = "{}#{}".format(PROTOCOL_USER_GET_HUMIDITY, "Humidity_Error")

    logger.info("sent back {} to socket: {}".format(message_back, websocket))
    await websocket.send(message_back)


async def auth_with_synology(username, password):
    global db
    response = json.loads(requests.get(SYNOLOGY_AUTH_URL,
                                       params={"api": "SYNO.API.Auth", "version": "2", "method": "login",
                                               "account": username,
                                               "passwd": password},
                                       verify=False).content)
    print(response)
    return response["success"], response["data"]["sid"]


ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
localhost_pem = pathlib.Path("cert/cert_priv.pem")
ssl_context.load_cert_chain(localhost_pem)

start_server = websockets.serve(
    message_handler, "localhost", 3000, ssl=ssl_context
)


def push_notification(title, msg, priority):
    with subprocess.Popen(
            ["curl", "-X", "POST", "https://elbxyz.synology.me:2001/message?token={}".format(GOTIFY_MESSAGING_TOKEN),
             "-F", "title={}".format(title), "-F",
             "message={}".format(msg), "-F", "priority={}".format(priority)],
            stdout=subprocess.PIPE) as proc:
        logger.info("Push Notification: {} sent result: {}".format(msg, proc.stdout.read()))


def is_sensor_in_triggered_group(sensor_name):
    global db
    groups = db.query(Group).all()
    for group in groups:
        if group.is_triggered and sensor_name in group.data.split(","):
            return group
    return None


def alarm_priority_handler(group):
    global is_about_to_alarm
    current_priority = get_current_priority()
    if current_priority == NOTIFICATION_PRIORITY_REALTIME and not is_about_to_alarm:
        push_notification("Realtime Alarm", "URGENT: Going to alarm in {} seconds".format(cache[CACHE_ALARM_DELAY]),
                          current_priority)
        is_about_to_alarm = True
        threading.Thread(target=timed_schedule_alarm, args=(group.name,)).start()

    elif current_priority == NOTIFICATION_PRIORITY_INSPECT_MODE:
        push_notification("Inspect Alarm", "INSPECT: Was going to alarm in {} seconds".format(cache[CACHE_ALARM_DELAY]),
                          current_priority)


def handle_receiver_messages(msg):
    global db
    global cache
    spl = None
    gpio_detected = None
    msg = msg.decode("UTF-8")
    if msg.startswith(PROTOCOL_PHYSICAL_DETECTION) or msg.startswith(PROTOCOL_WIRELESS_DETECTION):
        if msg.startswith(PROTOCOL_PHYSICAL_DETECTION):
            spl = msg.split(PROTOCOL_DELIMITER)
            gpio_detected = spl[1]
            pin_detected = spl[2]
            # Adding those automatic only cause they are connected physically by intention
            sensor_search = db.query(Sensor).filter_by(gpio=gpio_detected).first()

            if sensor_search is None:
                db.add(Sensor(name="Unnamed physical sensor GPIO {}".format(gpio_detected), gpio=gpio_detected,
                              pin=pin_detected, location="Unknown location",
                              sensor_type="Physical",
                              wireless_length=0))
                db.commit()
            else:
                logger.info(sensor_search)
            sensor_search = db.query(Sensor).filter_by(gpio=gpio_detected).first()
            triggered_group = is_sensor_in_triggered_group(sensor_search.name)
            if triggered_group is not None:
                push_notification("Physical Detection", "Detected physical sensor named {}".format(sensor_search.name),
                                  get_current_priority())
                alarm_priority_handler(triggered_group)

        else:
            spl = msg.split(PROTOCOL_DELIMITER)
            gpio_detected = spl[1]
            wireless_length_detected = spl[2]
            sensor_search = db.query(Sensor).filter_by(wireless_length=wireless_length_detected).first()
            # Activate notification only if the wireless wave is in the db
            if sensor_search is not None:
                triggered_group = is_sensor_in_triggered_group(sensor_search.name)
                if triggered_group is not None:
                    push_notification("Wireless Detection",
                                      "detected wireless sensor named {}".format(sensor_search.name),
                                      get_current_priority())
                    alarm_priority_handler(triggered_group)

    elif msg.startswith(PROTOCOL_TEMPERATURE):
        spl = msg.split(PROTOCOL_DELIMITER)
        # [1] is Temperature, [2] is humidity
        # Updating values
        cache[CACHE_TEMPERATURE] = spl[1]
        cache[CACHE_HUMIDITY] = spl[2]

    return "KEEPALIVE".encode("UTF-8")


def get_current_priority():
    global cache
    if cache[CACHE_IS_INSPECT_KEY]: return NOTIFICATION_PRIORITY_INSPECT_MODE
    return NOTIFICATION_PRIORITY_REALTIME


def send_to_receivers(msg):
    global receiver_sockets
    if type(msg) == str:
        msg = msg.encode("UTF-8")
    if receiver_sockets is not None:
        for s in receiver_sockets:
            with receiver_lock:
                s.send(msg)


# A server thread to handle receiver/s messages
def server_thread():
    global db
    global server
    global receiver_sockets
    # socket.setdefaulttimeout(5)
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setblocking(60)
    server.bind((HOST, 4005))
    server.listen(5)
    inputs = [server]
    outputs = []
    message_queues = {}
    while inputs:
        readable, writable, exceptional = select.select(inputs, outputs, inputs, 5)
        receiver_sockets = writable
        for s in readable:
            if s is server:
                connection, client_address = s.accept()
                connection.setblocking(0)
                inputs.append(connection)
                message_queues[connection] = queue.Queue()
            else:
                data = s.recv(1024)
                if data:
                    message_queues[s].put(data)
                    if s not in outputs:
                        outputs.append(s)
                else:
                    if s in outputs:
                        outputs.remove(s)
                    inputs.remove(s)
                    s.close()
                    del message_queues[s]

        for s in writable:
            try:
                next_msg = message_queues[s].get_nowait()
            except queue.Empty:
                outputs.remove(s)
            else:
                response = handle_receiver_messages(next_msg)
                with receiver_lock:
                    s.send(response)

        for s in exceptional:
            inputs.remove(s)
            if s in outputs:
                outputs.remove(s)
            s.close()
            del message_queues[s]


def scheduler_handler_thread():
    global db
    while True:
        schedules = db.query(Scheduler).all()
        now = datetime.datetime.now().strftime("%A-%H:%M")
        for schedule in schedules:
            if now == "{}-{}".format(schedule.day, schedule.time):
                group_search = db.query(Group).filter_by(name=schedule.group_name).first()
                if group_search:
                    if schedule.action == "Arm":
                        if not group_search.is_triggered:
                            group_search.is_triggered = True
                            db.commit()
                            threading.Thread(target=push_notification,
                                             args=("Group Arm",
                                                   "Sensors in group {} are armed".format(group_search.name),
                                                   get_current_priority()),
                                             ).start()
                    elif schedule.action == "Disarm":
                        if group_search.is_triggered:
                            group_search.is_triggered = False
                            db.commit()
                            threading.Thread(target=push_notification,
                                             args=("Group Disarm",
                                                   "Sensors in group {} are unarmed".format(group_search.name),
                                                   get_current_priority()),
                                             ).start()
        time.sleep(50)


def signal_handler(signal, frame):
    global server
    global cache
    logging.info("Caught Ctrl+C, shutting down...")
    server.close()
    cache.close()
    sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)
# Server to serve the receiver
_thread.start_new_thread(server_thread, ())
# Scheduler Handler thread
_thread.start_new_thread(scheduler_handler_thread, ())
# Server to serve clients (both external & Internal)
asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
